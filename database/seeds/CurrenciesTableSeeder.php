<?php

use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->insert([
            'id' => 1,
            'name' => 'EUR',
            'value'=> 1,
        ]);
        
        DB::table('currencies')->insert([
            'id' => 2,
            'name' => 'USD',
            'value'=> 0.9,
        ]);
        
        DB::table('currencies')->insert([
            'id' => 3,
            'name' => 'GBP',
            'value'=> 1.1,
        ]);
    }
}
