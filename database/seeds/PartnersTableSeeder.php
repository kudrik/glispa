<?php

use Illuminate\Database\Seeder;

class PartnersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {       
        DB::table('partners')->insert([
            'id' => 1,
            'name' => 'First',
            'method'=> 1,
        ]);
        
        DB::table('partners')->insert([
            'id' => 2,
            'name' => 'Second',
            'method'=> 2,
        ]);
                
        DB::table('partners')->insert([
            'id' => 3,
            'name' => 'Third',
            'method'=> 3,
        ]);
        
        DB::table('partners')->insert([
            'id' => 4,
            'name' => 'fourth',
            'method'=> 1,
        ]);
        
        
        DB::table('partners')->insert([
            'id' => 5,
            'name' => 'fifth',
            'method'=> 2,
        ]);        
    }
}
