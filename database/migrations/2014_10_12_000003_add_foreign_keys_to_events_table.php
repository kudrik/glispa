<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('events', function(Blueprint $table)
		{
			$table->foreign('partner_id', 'events_partner_ibfk')->references('id')->on('partners')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('currency_id', 'events_currency_ibfk')->references('id')->on('currencies')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('events', function(Blueprint $table)
		{
			$table->dropForeign('events_partner_ibfk');
			$table->dropForeign('events_currency_ibfk');
		});
	}

}
