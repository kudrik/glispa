<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    public $timestamps = false;
    
    protected $fillable = ['name', 'value'];
    
    public function events()
    {
        return $this->hasMany('App\Event');
    }
}