<?php

namespace App;

class Payment
{
    const delay = 4;
    const afterWeekendPaymentDay = 3;
    
    //count of rows for each procedure
    public $chunkSize = 10000;
    
    public function all()
    {
        $maps = array();       

        $partnersMethods = array();

        $currencies = array();
        
        $lastChunkId = 0;
        
        $process = true;
        
        while ($process) {
            
            $eventsObj = \DB::table('events')
                ->select('id','created_at','partner_id','amount','currency_id');
            
            if ($this->chunkSize>0) {
                $eventsObj->where('id', '>', $lastChunkId)->orderBy('id','ASC')->limit($this->chunkSize);               
            }
            
            $events = $eventsObj->get(); 
                                    
            if (!(count($events)>0)) {               
                break;
            }
            
            if ($this->chunkSize==0) {
                $process = false;
            }
            
            foreach ($events as $k=>$event) {
                
                $lastChunkId = $event->id;
            
                $date = strtotime($event->created_at);
                 
                $day = date('d',$date);
            
                $key = date('Y-m-',$date);
            
                //save partners method to cache array
                if (!isset($partnersMethods[$event->partner_id])) {
                    $partnersMethods[$event->partner_id] = \App\Partner::find($event->partner_id)->method;
                }
            
                //save currencies val to cache array
                if (!isset($currencies[$event->currency_id])) {
                    $currencies[$event->currency_id] = \App\Currency::find($event->currency_id)->value;
                }
            
                //if weekly
                if ($partnersMethods[$event->partner_id] == 1) {
            
                    if ($day<=7) {
                        $key.= '07';
                    } elseif ($day<=15) {
                        $key.= '15';
                    } elseif ($day<=23) {
                        $key.= '23';
                    } else {
                        $key.= date('t',$date);
                    }
                    //Bi­Weekly
                } elseif ($partnersMethods[$event->partner_id] == 2) {
                    if ($day<=15) {
                        $key.= '15';
                    } else {
                        $key.= date('t',$date);
                    }
                    //Monthly
                } else {
                    $key.= date('t',$date);
                }
            
                if (!isset($maps[$key])) {
                    $maps[$key] = array();
                }
            
                if (!isset($maps[$key][$event->partner_id])) {
                    $maps[$key][$event->partner_id] = 0;
                }
            
                $maps[$key][$event->partner_id] = $maps[$key][$event->partner_id] + $event->amount*$currencies[$event->currency_id];
                                            
                unset($events[$k]);
            }
        }        
                        
        ksort($maps);
        
        $result = array();
                
        foreach ($maps as $k=>$period) {
        
            $finalDate = strtotime($k) + self::delay*24*60*60;
        
            //check that it is not weekend
            $dayOfweek = date('w',$finalDate);
            if ($dayOfweek==0) {
                //find closer Wednesday
                $finalDate = $finalDate + self::afterWeekendPaymentDay*24*60*60;
            } elseif ($dayOfweek==6) {
                //find closer Wednesday
                $finalDate = $finalDate + (self::afterWeekendPaymentDay+1)*24*60*60;
            }
        
            foreach ($period as $partner=>$total) {
                $result[]=array(
                    'date'=>$finalDate,
                    'partner'=>$partner,
                    'total'=>$total,
                );
                
                unset($maps[$k][$partner]);
            }
            
            unset($maps[$k]);
        }        
        
        return $result;        
    }    
    
    public function AllViaSql()
    {    	
    	$events = \DB::table('events')
    	->select(
    			\DB::raw('@a:=CONCAT(
	    					DATE_FORMAT(events.created_at,"%Y-%m-"),
	    					(CASE
	    						WHEN partners.method=1 THEN
	    							(CASE
	    								WHEN DAY(events.created_at) BETWEEN 1 AND 7 THEN "07"
	    								WHEN DAY(events.created_at) BETWEEN 8 AND 15 THEN "15"
	    								WHEN DAY(events.created_at) BETWEEN 16 AND 23 THEN "23"
	    								ELSE DAY(LAST_DAY(events.created_at))
	    							END)
	    						WHEN partners.method=2 THEN
	    							(CASE
	    								WHEN DAY(events.created_at) BETWEEN 1 AND 15 THEN "15"    								
	    								ELSE DAY(LAST_DAY(events.created_at))
	    							END)
	    						ELSE DAY(LAST_DAY(events.created_at))
	    					END)
	    				) + INTERVAL '.self::delay.' DAY AS prePeriod, 
    					(CASE
    						WHEN DAYOFWEEK(@a)=1 THEN @a + INTERVAL '.self::afterWeekendPaymentDay.' DAY
    						WHEN DAYOFWEEK(@a)=7 THEN @a + INTERVAL '.(self::afterWeekendPaymentDay+1).' DAY
    						ELSE @a
    					END) AS date
    					'),   			
    			'partner_id',
    			\DB::raw('SUM(events.amount*currencies.value) AS total')
    			)
    	->join('currencies', 'currencies.id', '=', 'events.currency_id')
    	->join('partners', 'partners.id', '=', 'events.partner_id')
    	->groupBy('events.partner_id','date')
    	->orderBy('date')
    	->get();
    	
    	return $events;    	
    }
}