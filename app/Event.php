<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [ 'created_at', 'amount' ];
    
    public function currency()
    {
        return $this->belongsTo('App\Currency');
    }  
    
    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }
}