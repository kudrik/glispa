<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    public $timestamps = false;
    
    protected $fillable = ['id', 'name', 'method'];
    
    public function events()
    {
        return $this->hasMany('App\Event');
    }
}