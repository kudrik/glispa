<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Log;

class ImportEvents extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ImportEvents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import events';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \App\Event::truncate();
        
        // add currency
        if (($handle = fopen('resources/data.csv', "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                
                if (count($data) < 5 or ! ($data[2] > 0)) {
                    continue;
                }
                
                $currency = \App\Currency::firstOrCreate(array(
                    'name' => $data[4]
                ));
                
                if (! $partner = \App\Partner::find($data[2])) {
                    $partner = \App\Partner::create(array(
                        'id' => $data[2],
                        'name' => 'Partner number ' . $data[2],
                        'method' => intval($data[2] + 2) % 3 + 1
                    ));
                }
                
                $events = new \App\Event();
                $events->partner()->associate($partner);
                $events->currency()->associate($currency);
                $events->created_at = \DateTime::createFromFormat('d/m/Y H:i', $data[1]);
                $events->amount = $data[3];
                $events->save();
            }
        }
        
        $this->info('Data is loaded');
    }
}
