<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Log;

class Report extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Display report';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$name = $this->choice('Which algorithm to use?', ['phpWithChunks', 'php', 'mysql']);

    	$this->info('Payments to partners ('.$name.')');

    	$starttime = microtime(true);

    	//\DB::enableQueryLog();

    	$Payment = new \App\Payment();

    	if ($name == 'mysql') {        
        
    		foreach ($Payment->AllViaSql() as $row) {
    		
    			$this->line($row->date.' | '.$row->partner_id.' | '.$row->total);
    		}
        	
    	} else {    	    
    	    
    	    if ($name == 'php') {
    	        $Payment->chunkSize = 0;
    	    }

    		foreach ($Payment->all() as $row) {
    		
    			$this->line(date('Y-m-d',$row['date']).' | '.$row['partner'].' | '.$row['total']);
    		}    		
    	}
    	
    	//print_R(\DB::getQueryLog());
        
        $this->info('Time: '.(microtime(true) - $starttime) ); 
        $this->info('PHP Memory peak: '.memory_get_peak_usage() );
        
    }
}
