<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;


use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index(Request $request)
    {     
        $Payment = new \App\Payment();       
        
        return view('welcome',array('results'=>$Payment->all()));
    }  
}