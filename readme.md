## About Project

The project is based on Laravel 5.3

1) after clone run in directory 

```
#!python
composer install
```

2) create MYSQL DB and add credentials to config/database.php file in mysql sections

3) run migrations
```
#!python
php artisan migrate
```

4) load seeds (currency and fake partners)
```
#!python
php artisan db:seed
```
or load to DB data from glispaDataOnly.sql (it is data from csv)

5) Load events from cvs file. copy data.csv file to resources/data.csv and run
```
#!python
php artisan ImportEvents
```

6) you can run app from comand line like: 

```
#!python
php artisan report
```
--
Update for 24.12.2016:
Also  I added new methods which counts it via mysql query and via PHP but with chunks queries. And after run app you will have choice between methods - phpWithChunks, php or mysql.
--

if you want to load new data then return to p.5

you can change currency value in table currencies, field "value".

you can change partner's payment method in table partners - field "method" (1,2 or 3 value)